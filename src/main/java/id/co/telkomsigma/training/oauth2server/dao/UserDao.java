package id.co.telkomsigma.training.oauth2server.dao;

import id.co.telkomsigma.training.oauth2server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User,String> {

    User findByUsername(String username);
}
